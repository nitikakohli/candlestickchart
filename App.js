import React, { Component } from "react";
import { Container } from "native-base";
import { Dimensions } from "react-native";
var { width, height } = Dimensions.get("window");
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import "react-native-gesture-handler";
import Coinbasepro from "./src/CoinbasePro/CoinbasePro";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: height,
      width: width
    };
  }

  onValueChange(value) {
    this.setState({
      selected: value
    });
  }
  newJWT(jwt) {
    this.setState({
      jwt: jwt
    });
  }
  render() {
    return <Container></Container>;
  }
}
const AppNavigator = createStackNavigator(
  {
    Home: {
      screen: Coinbasepro
    },
   
  },
  {
    headerMode: "none",
    navigationOptions: {
      headerVisible: false
    }
  }
);
export default createAppContainer(AppNavigator);
