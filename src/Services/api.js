import axios from 'axios';
import config from '../config';
import {AsyncStorage} from 'react-native';


export const fetchApi = async function(endpoint, payload, headers, auth, method = 'get') {
  headers['Content-Type'] = 'application/json'
  if(auth ===true){
    var token = await AsyncStorage.getItem('sessionId')
    headers.Authorization = "token " + token;
  }

  
    const axiosConfig = {
      headers,
      method: method.toLowerCase(),
    };
    if (axiosConfig.method === 'get') {
      axiosConfig.params = payload;
    } else if (axiosConfig.method === 'delete') {
      axiosConfig.params = payload;
    } else {
      axiosConfig.data = payload;
    }
    
 
    return axios(config.url+ endpoint, axiosConfig);
  };
  