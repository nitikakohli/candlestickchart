"use strict";

var React = require("react-native");

var { StyleSheet } = React;

module.exports = StyleSheet.create({
  alwaysred: {
    backgroundColor: "red",
    height: 100,
    width: 100
  },

  BgPrimary: {
    backgroundColor: "#1e90ff"
  },
  BgSecondary: {
    backgroundColor: "#6fbd13"
  },
  ColorPrimary: {
    color: "#1e90ff"
  },
  ColorSeconday: {
    color: "#6fbd13"
  },
  LabelFontSize: {
    fontSize: 13
  },
  PlaceholderColor: {
    color: "#646161"
  },
  LabelColor: {
    color: "#a6a6a6"
  },
  UpdatedText: {
    color: "#707070"
  },

  AnswerFontSize: {
    fontSize: 11
  },
  StudentFont: {
    fontSize: 12
  },
  InputHolder: {
    fontSize: 13
  },
  HeaderBgColor: {
    color: "#fff"
  },
  FooterBgColor: {
    backgroundColor: "#fff"
  },
  BgcolorNotification: {
    backgroundColor: "#e55934"
  },
  BgcolorQueries: {
    backgroundColor: "#fec01d"
  },
  searchBarColor: {
    color: "#b1aaaa"
  },
  ButtonText: {
    fontSize: 9
  },
  BulkColor: {
    backgroundColor: "#731963"
  },
  BulkTextColor: {
    color: "#731963"
  },
  AddStudent: {
    backgroundColor: "#fec01d"
  },
  AddStudentText: {
    color: "#fec01d"
  },
  ResetData: {
    backgroundColor: "#f6401b"
  },
  ResetText: {
    color: "#f6401b"
  },
  InstructionText: {
    fontSize: 18
  },
  LargeFont: {
    fontSize: 22
  },
  NoteText: {
    fontSize: 10
  },
  CsvFont: {
    fontSize: 15
  },
  VerbalPara: {
    fontSize: 8
  },
  postColor: {
    color: "#242021"
  },
  boxShadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowOpacity: 0.2,
    shadowRadius: 4.65,

    elevation: 7
  },
  BuckFont: {
    fontSize: 25
  },
  BgThird: {
    backgroundColor: "#6fc451"
  },
  quizFont: {
    fontSize: 20
  },
  Bgfourth: {
    backgroundColor: "#e55934"
  },
  BgFivth: {
    backgroundColor: "#731963"
  },
  BgSixth: {
    backgroundColor: "#11998e"
  },
  DailyQuizFont: {
    fontSize: 40
  },
  QuizFont: {
    fontSize: 55
  },
  fontQuiz: {
    fontSize: 50
  },
  UizFont: {
    fontSize: 19
  },
  QusetionFont: {
    fontSize: 16
  },
  finishedLine: {
    fontSize: 30
  },
  TimeFinish: {
    fontSize: 35
  },
  quizText: {
    fontSize: 27
  },
  score: {
    fontSize: 14
  },
  MainColor: {
    backgroundColor: "#fafafa"
  },
  scoreColor: {
    backgroundColor: "#cbcbcb"
  },
  WeeklyColor: {
    color: "#f5515f"
  },
  WeeklyQuizColor: {
    backgroundColor: "#f5515f"
  },
  DoubtHeader: {
    backgroundColor: "#11998e"
  },
  HomeFont: {
    fontSize: 23
  },
  CText: {
    fontSize: 29
  },
  questionFont: {
    fontSize: 24
  },
  ResetC: {
    fontSize: 34
  },
  font: {
    fontSize: 21
  },
  pervColor: {
    backgroundColor: "#669bf3"
  },
  shopiingFont: {
    color: "#2a2a2a"
  },
  signUpColor: {
    color: "#323232"
  },
  textColor: {
    color: "#3c3c3c"
  },
  cardFont: {
    fontSize: 17
  },
  discountColor: {
    color: "#919191"
  },
  totalHeading: {
    color: "#585b5e"
  },
  greencolor: {
    color: "#6fc451"
  },
  dicountColor: {
    color: "#434343"
  },
  ordercolor: {
    color: "#2ebe42"
  }
});
