import React, { Component } from "react";
import {
  Container,
  View,
  Thumbnail,
  Header,
  Left,
  Right,
  Icon,
  Form,
  Input,
  Item,
  Body,
  Button,
  Content,
  Text
} from "native-base";
import { AppLoading } from "expo";
import { StyleSheet, Dimensions, Keyboard } from "react-native";
import * as Font from "expo-font";
var { width, height } = Dimensions.get("window");
import { Ionicons } from "@expo/vector-icons";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import { TouchableOpacity } from "react-native-gesture-handler";
import global from "../AppCss";
import { fetchApi } from "../Services/api";
import md5 from "react-native-md5";
import { AsyncStorage } from "react-native";
import { StatusBar, Platform } from "react-native";
var headers = {
  "Content-Type": "application/json"
};
export default class AuthSignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: hp,
      height: height,
      width: width,
      isReady: false,
      fontLoaded: false,
      loading: true,
      dataSource: [],
      userEmail: "",
      username: "",
      password: "",
      Error: "",
      Errora: "",
      show: true,
      Errorb: "",
      firstName: "nitika",
      lastName: "kohli",
      referCode: ""
    };
    this._retrieveData();
  }

  componentWillMount() {
    this._retrieveData();
  }
  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require("../../node_modules/native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("../../node_modules/native-base/Fonts/Roboto_medium.ttf"),
      OpenSansRegular: require("../../assets/fonts/OpenSans-Regular.ttf"),
      OpenSansSemiboldItalic: require("../../assets/fonts/OpenSans-SemiboldItalic.ttf"),
      NunitoRegular: require("../../assets/fonts/Nunito-Regular.ttf"),
      OpenSansSemibold: require("../../assets/fonts/OpenSans-Semibold.ttf"),
      OpenSansLight: require("../../assets/fonts/OpenSans-Light.ttf"),
      ...Ionicons.font
    });

    this.setState({ isReady: true });
    // this._retrieveData();
    const { navigation } = this.props;
    if (navigation.getParam("sessionId")) {
      headers["Authorization"] = "token " + navigation.getParam("sessionId");
    } else {
      var value = await AsyncStorage.getItem("sessionId");
      headers["Authorization"] = "token " + value;
    }

    fetchApi("/webindex", {}, headers, false, "get")
      .then(response => {
        if (response.data.userData && response.data.userData.userId) {
          AsyncStorage.setItem(
            "userData",
            JSON.stringify(response.data.userData)
          );
          AsyncStorage.setItem("sessionId", response.data.userData.sessionId);
          this.props.navigation.navigate("cartdetail");
        } else {
          //  this.props.navigation.navigate('Home');
        }
      })
      .catch(err => {
        console.log(err);
      });
  }
  getUserId = async () => {
    let userId = "";
    try {
      userId = (await AsyncStorage.getItem("userId")) || "none";
    } catch (error) {
      console.log(error.message);
    }
    return userId;
  };
  _storeData = async sessionId => {
    try {
      await AsyncStorage.setItem("sessionId", sessionId);
      this.props.navigation.navigate("Login", {
        sessionId: sessionId
      });
    } catch (error) {
      console.log(error);
    }
  };
  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem("sessionId");

      if (value !== null) {
        headers["Authorization"] = "token " + value;
        this.setState({
          token: value
        });
        return value;
      }
    } catch (error) {
      return null;
    }
  };
  signUp = () => {
    let loginObj = {
      userEmail: this.state.userEmail,
      username: this.state.username,
      password: md5.hex_md5(this.state.password),
      role: "user",
      firstName: "jayyy",
      lastName: "looo",
      referCode: this.state.referCode
    };
    fetchApi("/signup/registerUser", loginObj, {}, false, "post")
      .then(response => {
        AsyncStorage.clear();
        if ((response.data.message = "success")) {
          console.log("rohan", response.data);
          if (response.data.success == false) {
            // alert("Invalid Details");
            this.handleValidation();
          } else {
            AsyncStorage.setItem(
              "userData",
              JSON.stringify(response.data.userData)
            );
            AsyncStorage.setItem("sessionId", response.data.userData.sessionId);
            alert("thankYou your details has been submitted");
            this.props.navigation.navigate("Login");
          }
        }
      })
      .catch(err => console.log(err));
  };

  InputValue = (key, val) => {
    this.setState({
      [key]: val
    });
  };
  handleValidation = () => {
    const { username, userEmail, password } = this.state;
    if (username == "") {
      this.setState({ Error: "please fill the username" });
    }
    if (username.length < 5) {
      this.setState({ Error: "username too small" });
    }
    if (userEmail == "") {
      this.setState({ Errorb: "please fill the email correct" });
    }
    if (password == "") {
      this.setState({ Errora: "please enter password" });
    }
    if (password.length > 2 && password.length < 10) {
      this.setState({ Errora: "password too small" });
    } else {
      alert("Invalid Details");
    }
    Keyboard.dismiss();
  };
  render() {
    if (!this.state.isReady) {
      return <AppLoading />;
    }

    return (
      <Container>
        <Header style={[global.FooterBgColor, styles.apple]}>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("Login")}
            >
              <Icon
                name="arrow-back"
                style={[styles.arrowButton, global.UpdatedText]}
              />
            </Button>
          </Left>
          <Body />
          <Right />
        </Header>
        <Content>
          <View>
            <Text
              style={[
                styles.signupTextWrapper,
                global.finishedLine,
                global.signUpColor
              ]}
            >
              Signup
            </Text>
          </View>
          {/* <View>
                        <Text style={[styles.joinTextWrapper, global.QusetionFont, global.textColor]}>Join us via :</Text>
                    </View> */}
          <View style={styles.twoButtonWrapper}>
            <TouchableOpacity style={[styles.firstFacbookButton]}>
              <Thumbnail
                small
                square
                source={require("../../assets/images/facebook.png")}
                style={styles.facebookIcon}
              />
              <Text
                style={[styles.facbookText, global.score, global.ColorPrimary]}
              >
                Facebook
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.firstFacbookButton]}>
              <Thumbnail
                small
                square
                source={require("../../assets/images/facebook.png")}
                style={styles.facebookIcon}
              />
              <Text
                style={[styles.facbookText, global.score, global.ColorPrimary]}
              >
                Google
              </Text>
            </TouchableOpacity>
          </View>
          <View>
            <Text
              style={[styles.signText, global.QusetionFont, global.textColor]}
            >
              Sign up with email or mobile number :
            </Text>
          </View>
          <View>
            <Form style={styles.formContainer}>
              <Item style={styles.lineWrapper}>
                <Input
                  style={[styles.formItem, global.score, global.LabelColor]}
                  placeholder="Full name"
                  value={this.state.username}
                  onChange={e =>
                    this.InputValue("username", e.nativeEvent.text)
                  }
                  id="fullname"
                />
              </Item>
              <Text style={[styles.errorMessage, global.StudentFont]}>
                {this.state.Error}
              </Text>
              <Item style={styles.lineWrapper}>
                <Input
                  style={[styles.formItem, global.score, global.LabelColor]}
                  placeholder="Email"
                  value={this.state.userEmail}
                  onChange={e => {
                    this.InputValue("userEmail", e.nativeEvent.text);
                  }}
                />
              </Item>
              <Text style={[styles.errorMessage, global.StudentFont]}>
                {this.state.Errorb}
              </Text>
              <Item style={styles.lineWrapper}>
                <Input
                  placeholder="Password"
                  style={[styles.formItem, global.score, global.LabelColor]}
                  value={this.state.password}
                  secureTextEntry={this.state.show}
                  onChange={e => {
                    this.InputValue("password", e.nativeEvent.text);
                  }}
                />
                <TouchableOpacity
                  style={styles.formIcon}
                  onPress={() =>
                    this.setState({
                      show: !this.state.show
                    })
                  }
                >
                  <Icon name="eye" style={styles.formIcon} />
                </TouchableOpacity>
              </Item>
              <Text style={[styles.errorMessage, global.StudentFont]}>
                {this.state.Errora}
              </Text>
              <Item style={styles.lineWrapper}>
                <Input
                  placeholder="ReferCode"
                  style={[styles.formItem, global.score, global.LabelColor]}
                  value={this.state.referCode}
                  onChange={e => {
                    this.InputValue("referCode", e.nativeEvent.text);
                  }}
                />
              </Item>
            </Form>
          </View>
          <Button
            block
            style={[styles.buttonSignUp, global.BgPrimary]}
            onPress={() => {
              this.signUp();
            }}
          >
            <Text
              style={[
                styles.signTextWrapper,
                global.QusetionFont,
                global.HeaderBgColor
              ]}
              uppercase={false}
            >
              Sign up
            </Text>
          </Button>
          <View style={[styles.accountWrapper]}>
            <Text style={[styles.accountTextWrapper, global.QusetionFont]}>
              Already have an account ?
            </Text>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Login")}
            >
              <Text
                style={[
                  styles.loginTextWrapper,
                  global.textColor,
                  global.QusetionFont
                ]}
              >
                Log in
              </Text>
            </TouchableOpacity>
          </View>
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  arrowButton: {
    color: "#404040"
  },
  errorMessage: {
    color: "red",
    fontFamily: "OpenSansRegular",
    marginLeft: wp("3%")
  },
  signupTextWrapper: {
    fontFamily: "OpenSansRegular",
    marginLeft: wp("6.4%"),
    marginTop: hp("1.2%")
  },
  joinTextWrapper: {
    fontFamily: "OpenSansRegular",
    marginLeft: wp("6.4%"),
    marginTop: hp("5%")
  },
  twoButtonWrapper: {
    display: "none",
    flexDirection: "row"
  },
  firstFacbookButton: {
    height: hp("5.7%"),
    width: wp("36.2%"),
    borderColor: "#667eea",
    display: "flex",
    flexDirection: "row",
    marginLeft: wp("6.4%"),
    marginTop: hp("3%"),
    borderWidth: 2,
    borderRadius: 4
  },
  facebookIcon: {
    resizeMode: "contain",
    height: hp("3%"),
    marginTop: hp("1.2%"),
    marginLeft: wp("2%")
  },
  facbookText: {
    fontFamily: "OpenSansRegular",
    marginTop: hp("1.2%")
  },
  signText: {
    fontFamily: "OpenSansRegular",
    justifyContent: "center",
    textAlign: "left",
    marginLeft: wp("6.4%"),
    marginTop: hp("5%")
  },
  formContainer: {
    marginTop: 0.046 * height,
    marginLeft: wp("5%"),
    marginRight: wp("8.5%")
  },
  formItem: {
    fontFamily: "OpenSansRegular",
    fontSize: 13,
    marginRight: 8.266,
    marginTop: 4.589
  },
  apple: {
    marginTop: StatusBar.currentHeight
  },
  formInput: {
    marginTop: 4.589,
    marginBottom: -hp("8%")
  },
  formIcon: {
    color: "#646161",
    marginBottom: -hp("0.5%")
  },
  lineWrapper: {
    marginLeft: wp("3%"),
    marginRight: wp("2%"),
    color: "#fafafa"
  },
  buttonSignUp: {
    width: wp("87.4%"),
    marginLeft: wp("6.4%"),
    marginTop: hp("4%"),
    borderRadius: 4
  },
  signTextWrapper: {
    fontFamily: "OpenSansRegular"
  },
  accountWrapper: {
    display: "flex",
    flexDirection: "row",
    marginTop: hp("3%"),
    justifyContent: "center",
    textAlign: "center",
    marginBottom: hp("2%")
  },
  accountTextWrapper: {
    color: "#a3a3a3",
    fontFamily: "OpenSansRegular"
  },
  loginTextWrapper: {
    marginLeft: wp("1%")
  }
});
